# coding=utf-8
import pytest
from atf import *
from atf.api import *
from route import Route


class TestCheckOptimizeRoute(TestCase):
    """
    Проверяется построитель маршрутов на сервисе sbis-osrm
    Строится маршрут между несколькими точками и проверяется факт пересечения
    с этлонной линией.
    """

    @classmethod
    def setUpClass(cls):
        # Логинимся на сайте для обращения к БЛ от пользователя USER
        cls.client = JsonRpcClient(cls.config.get("SITE"), verbose_log=1)
        cls.client.auth(cls.config.get("USER_NAME"), cls.config.get("PASSWORD"))

    @file_data("segments,skip_tool_roads,for_truck,quadkeys", "test_files/optimize_route.csv",
               encoding="utf-8")
    def test_optimize_route_01(self, segments, skip_tool_roads, for_truck, quadkeys):
        """
        Проверяется оптимизация маршрута
        :param segments: полученные точки начального маршрута
        :param skip_tool_roads: платные дороги
        :param for_truck: дороги для грузовых машин от 8т
        :param optimal_route: эталонный оптимизированный маршрут
        :param quadkeys: квадкеи маршрута
        """

        segments = eval(segments)
        skip_tool_roads = eval(skip_tool_roads)
        for_truck = eval(for_truck)
        quadkeys = dict(eval(quadkeys))
        quadkeys_list = []

        for key in quadkeys:
            quadkeys_list.append(quadkeys[key])

        log('Получаем дистанцию и время по изначальному маршруту ' + str(quadkeys_list))
        initial_route = Route(self.client).by_quadkeys(quadkeys_list, skip_tool_roads, for_truck)
        initial_distance = float(initial_route['distance'])
        log('Дистанция изначального маршрута: ' + str(initial_distance) + ' км.')
        initial_time = float(initial_route['time'])
        log('Время изначального маршрута: ' + str(initial_time) + ' мин.')
        quadkeys_list.clear()

        log('Оптимизируем маршрут')
        get_optimal_route = Route(self.client).optimize(segments, skip_tool_roads, for_truck)
        log('Получаем квадкоды по оптимизированному маршруту')
        for i in range(len(get_optimal_route['Points'])):
            quadkeys_list.append(quadkeys[get_optimal_route['Points'][i]])

        log('Получаем дистанцию и время по оптимизированному маршруту ' + str(quadkeys_list))
        optimal_route = Route(self.client).by_quadkeys(quadkeys_list, skip_tool_roads, for_truck)
        optimal_distance = float(optimal_route['distance'])
        log('Дистанция оптимизированного маршрута: ' + str(optimal_distance) + ' км.')
        optimal_time = float(optimal_route['time'])
        log('Время оптимизированного маршрута: ' + str(optimal_time) + ' мин.')

        delta_time = float(initial_time) - float(optimal_time)
        delta_distance = float(initial_distance) - float(optimal_distance)
        if initial_time < optimal_time:
            raise Exception(f"Время оптимизированного маршрута {optimal_time} больше изначального {initial_time}")
        elif float(initial_distance) < float(optimal_distance):
            raise Exception(f"Дистанция оптимизированного маршрута {optimal_distance} больше изначальной "
                            f"{initial_distance}")
        else:
            log('Маршрут оптимизирован. Дистанция уменьшилась на ' + str(round(delta_distance, 2)) +
                ' км. Время сократилось на ' + str(round(delta_time, 2)) + ' мин.')

    @file_data("segments,skip_tool_roads,for_truck, initial_distance, initial_time, optimal_distance, optimal_time, "
               "deviation_distance, deviation_time", "test_files/optimize_time_and_distance.csv",
               encoding="utf-8")
    def test_optimize_route_02(self, segments, skip_tool_roads, for_truck, initial_distance, initial_time,
                               optimal_distance, optimal_time, deviation_distance, deviation_time):
        """
                Проверяются время и дистанция оптимизированного маршрута
                :param segments: полученные точки начального маршрута
                :param skip_tool_roads: платные дороги
                :param for_truck: дороги для грузовых машин от 8т
                :param initial_distance: начальная дистанция маршрута
                :param initial_time: начальное время маршрута
                :param optimal_distance: дистанция оптимизированного маршрута
                :param optimal_time: время оптимизированного маршрута
                :param deviation_time: допустимое отклонение по времени
                :param deviation_distance: допустимое отклонение по дистанции
                """

        segments = eval(segments)
        skip_tool_roads = eval(skip_tool_roads)
        for_truck = eval(for_truck)
        log('Оптимизируем маршрут')
        get_optimal_route = Route(self.client).optimize(segments, skip_tool_roads, for_truck)
        result_initial_distance = round(((float(get_optimal_route['Initial']['distance']) / 1000)), 1)
        result_initial_duration = round(((float(get_optimal_route['Initial']['duration']) / 60)), 1)
        result_final_distance = round(((float(get_optimal_route['Final']['distance']) / 1000)), 1)
        result_final_duration = round(((float(get_optimal_route['Final']['duration']) / 60)), 1)
        log("Сравниваем эталонные время и дистанцию с тем, что получили в методе")
        delta_initial_distance = abs(float(initial_distance) - result_initial_distance)
        delta_initial_time = abs(float(initial_time) - result_initial_duration)
        delta_optimal_distance = abs(float(optimal_distance) - result_final_distance)
        delta_optimal_time = abs(float(optimal_distance) - result_final_distance)
        if delta_initial_distance > float(deviation_distance):
            raise Exception(
                f"Эталонная дистанция изначального маршрута {initial_distance}  не совпадает с ответом метода "
                f"{result_initial_distance}")
        if delta_initial_time > float(deviation_time):
            raise Exception(
                f"Эталонное время изначального маршрута {initial_time} не совпадает с ответом метода "
                f"{result_initial_duration}")
        if delta_optimal_distance > float(deviation_distance):
            raise Exception(
                f"Эталонная дистанция оптимизированного маршрута {optimal_distance} не совпадает с ответом метода "
                f"{result_final_distance}")
        if delta_optimal_time > float(deviation_time):
            raise Exception(
                f"Эталонное время оптимизированного маршрута  {optimal_time} не совпадает с ответом метода "
                f"{result_final_duration}")
