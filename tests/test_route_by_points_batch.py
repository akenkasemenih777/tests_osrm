# coding=utf-8

import pytest
from atf import *
from atf.api import *
from route import Route


class TestByPointsBatch(TestCase):
    """
        Проверяется построение нескольких маршрутов по заданному набору координат на сервисе sbis-osrm
        Для заданного набора точек получаем самый оптимальный маршрут и время, дистанцию в км
        Полученные дистанция и время сравниваются с эталонными в пределах погрешности

        """

    @classmethod
    def setUpClass(cls):
        # Логинимся на сайте для обращения к БЛ от пользователя USER
        cls.client = JsonRpcClient(cls.config.get("SITE"))
        cls.client.auth(cls.config.get("USER_NAME"), cls.config.get("PASSWORD"))

    @file_data("points_list,time,diff_time,dist,diff_dist", "test_files/get_routes_batch.csv", encoding="utf-8")
    def test_by_points_batch_01(self, points_list, time, diff_time, dist, diff_dist):

        """
               Проверяется построение нескольких маршрутов по заданному набору координат
               и сравнение полученных длины и времени маршрутов с эталонными
               :param points_list: набор координат для построения маршрутов
               :param time: ожидаемое время маршрута (эталон)
               :param diff_time: допустимое отклонение по времени
               :param dist: ожидаемая длинна маршрута (эталон)
               :param diff_dist допустимое отклонение по расстоянию
               """

        points_list = list(eval(points_list))
        time = list(eval(time))
        dist = list(eval(dist))
        diff_time = float(diff_time)
        diff_dist = float(diff_dist)
        log('Получаем маршруты по заданному набору координат ' + str(points_list))
        response = Route(self.client).by_points_batch(points_list)
        count = 0
        if response:
            log('Сравниваем полученные маршруты')
            for route in response:
                log('Получен маршрут со временем ' + str(route.get('t')) + ' мин и расстоянием ' + str(
                    route.get('dist')) + ' км.')
                dt = route.get('t') - time[count]
                dd = route.get('dist') - dist[count]
                if abs(dt) > diff_time:
                    raise Exception(f"Полученное время автомобильного маршрута {route.get('t')} мин отличается "
                                    f"от эталонного {time[count]} мин, допустимая погрешность {diff_time} мин. "
                                    f"Исходный маршрут {points_list[count]} ")
                elif abs(dd) > diff_dist:
                    raise Exception(f"Полученная длина автомобильного маршрута {route.get('dist')} км отличается "
                                    f"от эталонного {dist[count]} км, допустимая погрешность {diff_dist} км. "
                                    f"Исходный маршрут {points_list[count]} ")
                else:
                    log('Полученные время и расстояние маршрута в пределах допустимых погрешностей')
                count = count + 1
        else:
            raise Exception(f"Некорректный ответ от сервиса, исходный маршрут: {points_list}; ")
