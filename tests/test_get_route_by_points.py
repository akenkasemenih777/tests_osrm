# coding=utf-8
import pytest
from atf import *
from atf.api import *
from route import Route


class TestRouteByPoints(TestCase):
    """
    Проверяется построение маршрутов по заданному набору координат на сервисе sbis-osrm
    Для заданного набора точек получаем самый оптимальный маршрут, дистанцию в км
    Полученная дистанция сравнивается с эталонной в пределах погрешности
    """

    @classmethod
    def setUpClass(cls):
        # Логинимся на сайте для обращения к БЛ от пользователя USER
        cls.client = JsonRpcClient(cls.config.get("SITE"))
        cls.client.auth(cls.config.get("USER_NAME"), cls.config.get("PASSWORD"))

    @file_data("points_list,distance,deviation", "test_files/get_route_by_points.csv", encoding="utf-8")
    def test_route_by_points(self, points_list, distance, deviation):
        """
        Проверяется построение маршрута по заданному набору координат
        и сравнение полученной длинны маршрута с эталонной
        :param points_list: набор координат для построения маршрута
        :param distance: ожидаемая длинна маршрута (эталон)
        :param deviation: допустимое отклонение по расстоянию
        """

        points_list = list(eval(points_list))
        log('Получаем маршрут по точкам: ' + str(points_list))
        response = Route(self.client).by_points(points_list, 0)

        if response:
            log('Получен маршрут длиной '+ str(response['distance'])+'км')
            route_diff = abs((float(response['distance']) - float(distance)))
            # модуль разности между полученным маршрутом и эталонным
            if route_diff > float(deviation):
                raise Exception(f"Полученная длинна маршрута {response['distance']}км отличается "
                                f"от эталонной {distance}км допустимая погрешность {deviation}км. "
                                f"Исходный маршрут {points_list}")
            else:
                log("Длина маршрута в пределах допустимой погрешности.")
        else:
            raise Exception(f"Некорректный ответ от сервиса, исходный маршрут: {points_list}")