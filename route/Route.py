from common_api import CommonApi


class Route(CommonApi):
    """В классе описаны API-методы объекта Route"""

    def __init__(self, client):
        super().__init__(client)

    def by_points(self, points_list, route_type):
        """
        Метод построения маршрута по заданному набору точек
        :param route_type: тип маршрута (0 автомобильный, 1 пеший)
        :param points_list: список содержаший наборы координат
        :return: построенный маршрут, длинна пути в км, время в пути
        """

        params = {"Points": points_list,
                  "Params": {
                      "d": [True, 6, route_type, False, True],
                      "s": [{"t": "Логическое", "n": "CalcIntersections"},
                            {"t": "Число целое", "n": "PolylinePrecision"},
                            {"t": "Число целое", "n": "RouteType"},
                            {"t": "Логическое", "n": "SkipRoute"},
                            {"t": "Логическое", "n": "RoundCorners"}],
                      "_type": "record",
                      "f": 0
                  }}

        result = self.client.call_rvalue("Route.ByPoints", path="/sbis-osrm/service", **params).result
        return result

    def by_quadkeys(self, quadkeys_list, skip_tool_roads, for_truck):
        """
        Метод построения маршрута по заданному набору точек
        :param quadkeys_list: список содержаший наборы квадкодов
        :param skip_tool_roads: True - не учитывает платные дороги, False - работает со всеми дорогами
        :param for_truck - true - применяем оптимизацию для грузовика - более 8т, кроме грузовиков
        :return: построенный маршрут, длинна пути в км, время в пути
        """

        params = {"QuadKeys": quadkeys_list,
                  "Params": {
                      "d": [False, skip_tool_roads, for_truck, False],
                      "s": [{"t": "Логическое", "n": "CalcIntersections"},
                            {"t": "Логическое", "n": "SkipTollRoads"},
                            {"t": "Логическое", "n": "ForTrucks"},
                            {"t": "Логическое", "n": "ContinueStraight"}],
                      "_type": "record",
                      "f": 0
                  }}

        result = self.client.call_rvalue("Route.ByQuadKeys", path="/sbis-osrm/service", **params).result
        return result

    def geometry_object_intersect(self, raw_geo_json, checking_geo_json):
        """
        Метод проверки существования пересечений между
        2 объектами в формате geo json
        :param raw_geo_json: json c геометрией первого объекта
        :param checking_geo_json: json с геометрией второго маршрута
        :return: True если есть пересечения, False если нет пересечений
        """

        params = {
            "ObjectA": raw_geo_json,
            "ObjectB": checking_geo_json}
        result = self.client.call_rvalue("Geometry.ObjectsIntersect",
                                         path="/geo-provider/service",
                                         **params).result
        return result

    def by_points_batch(self, points):
        """
        Метод для построения маршрутов из списка точек
         :param points: список содержаший наборы координат
        :return: построенный маршрут, длинна пути в км, время в пути
        """
        params = {"Points": points,
                  "Params": {
                      "d": [True, 6, 0, False],
                      "s": [{"t": "Логическое", "n": "CalcIntersections"},
                            {"t": "Число целое", "n": "PolylinePrecision"},
                            {"t": "Число целое", "n": "RouteType"},
                            {"t": "Логическое", "n": "SkipRoute"}],
                      "_type": "record",
                      "f": 0
                  }}
        result = self.client.call_rvalue("Route.ByPointsBatch", path="/sbis-osrm/service", **params).result
        return result

    def optimize(self, segments, skip_tool_roads=True, for_truck=False):
        """

               Метод для оптимизации маршрута из списка точек
                :param skip_tool_roads:
                :param segments: список точек маршрута
                :param skip_tool_roads: True - не учитывает платные дороги, False - работает со всеми дорогами
                :param for_truck - true - применяем оптимизацию для грузовика - более 8т, кроме грузовиков
                false - только легковые авто
               :return: Оптимизированную последовательность
               """
        params = {"Points": segments,
                  "Params": {
                      "d": [skip_tool_roads, for_truck],
                      "s": [{"n": "SkipTollRoads", "t": "Логическое"},
                            {"n": "ForTruck", "t": "Логическое"}],
                      "_type": "record",
                      "f": 0
                  }
                  }
        result = self.client.call_rvalue("Route.Optimize", path="/sbis-osrm/service/?srv=1", **params).result
        return result
