"""
В файле описан класс, от которого необходимо наследоваться при описании методов API используемых в проекте inside
"""
from atf.api import JsonRpcClient


class CommonApi:
    """Класс для общих методов обработки взаимодействия в API"""

    def __init__(self, client=None, url=None, sid=None):
        """
        :param client: Экземпляр класса JsonRpcClient
        :param url: url, к которому будем посылать посылать API запросы, указывается вместе с sid
        :param sid: id сессии, с которым создаем экземпляр класса JsonRpcClient, указывается вместе с url
        """

        if client:
            assert isinstance(client, JsonRpcClient), f"В класс был передан неверный экземпляр " \
                                                      f"{type(client).__name__}" \
                                                      "класса отличный от JsonRpcClient"
            self.client = client
        elif url and sid:
            self.client = JsonRpcClient(url, sid=sid)
        else:
            raise ValueError("Если в класс не передан client, то необходимо указать url и sid")